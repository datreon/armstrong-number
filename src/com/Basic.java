package com;
import java.util.*;

    public class Basic {
        public static void main(String[] args) {
            System.out.println("hello from Main");
            int n, sum = 0, inputNumber, remainder, digits = 0;

            Scanner in = new Scanner(System.in);
            System.out.println("Input a number to check if it is an Armstrong number");
            inputNumber = in.nextInt();

            n = inputNumber;

// Count number of digits

            digits = (int) (Math.log10(inputNumber) + 1);

            System.out.println("digits from log = " + digits);

            Double power = new Double(0);
            while (n != 0) {
                remainder = n % 10;
                System.out.println("while loop  : inputNumber = " + n + " remainder = " + remainder);
                power = Math.pow(remainder, digits);
                sum = sum + power.intValue();
                System.out.println("sum: " + sum);
                n = n / 10;

                if (inputNumber == sum)
                    System.out.println(inputNumber + " is an Armstrong number.");
                else
                    System.out.println(inputNumber + " is not an Armstrong number.");
            }
        }
    }
